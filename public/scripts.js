//Constants//
const request = new XMLHttpRequest();
const baseURI = 'https://www.handyfeeling.com/api/handy/v2';
const modeAPI = '/mode';
const hampStartAPI = '/hamp/start';
const hampStopAPI = '/hamp/stop';
const hampVelocityAPI = '/hamp/velocity';
const slideAPI = '/slide';

const clearAllTimeouts = createClearAllTimeouts();


//Current Technique variables - to be deprecated for the array//
var minHoldSlide = 6000;
var maxHoldSlide = 6000;
var minBottomSlide = 0;
var maxBottomSlide = 0;
var minTopSlide = 100;
var maxTopSlide = 100;
var minHoldVelocity = 3000;
var maxHoldVelocity = 3000;
var minVelocity = 6;
var maxVelocity = 6;

const techniques = {name:"undefined",liklihood:0, minHoldSlide:6000, minBottomSlide:6000
	, maxBottomSlide:0, minTopSlide:0, maxTopSlide:100
	, minHoldVelocity:3000, maxHoldVelocity:3000, minVelocity:6
	, maxVelocity:6};


// calls both setSlide and setVelocity using presets from html page
async function setTechnique(buttonID) {
	// set all the variables to use whatever the input technique sends
	minHoldSlide = parseFloat(document.getElementById(buttonID).getAttribute('data-minHoldSlide'));
	maxHoldSlide = parseFloat(document.getElementById(buttonID).getAttribute('data-maxHoldSlide'));
	minBottomSlide = parseFloat(document.getElementById(buttonID).getAttribute('data-minBottomSlide'));
	maxBottomSlide = parseFloat(document.getElementById(buttonID).getAttribute('data-maxBottomSlide'));
	minTopSlide = parseFloat(document.getElementById(buttonID).getAttribute('data-minTopSlide'));
	maxTopSlide = parseFloat(document.getElementById(buttonID).getAttribute('data-maxTopSlide'));
	minHoldVelocity = parseFloat(document.getElementById(buttonID).getAttribute('data-minHoldVelocity'))
	maxHoldVelocity = parseFloat(document.getElementById(buttonID).getAttribute('data-maxHoldVelocity'))
	minVelocity = parseFloat(document.getElementById(buttonID).getAttribute('data-minVelocity'));
	maxVelocity = parseFloat(document.getElementById(buttonID).getAttribute('data-maxVelocity'));
	
	clearAllTimeouts();
	
	console.log("Loading New Technique");
	
	// set the slide value and wait for completion of the async function
	let slidePromise = new Promise((resolve, reject) => {
		resolve(setSlide());
	});
	let slideResult = await slidePromise;
	
	// set the velocity value and wait for completion of the async function
	let velocityPromise = new Promise((resolve, reject) => {
		resolve(setVelocity());
	});
	let velocityResult = await velocityPromise;	
	
	console.log("New Technique Loaded");
};

/*
// calls both setSlide and setVelocity using presets from html page
async function setTechnique(minBottomSlideInput, maxBottomSlideInput, minTopSlideInput, maxTopSlideInput, minVelocityInput, maxVelocityInput, minHoldSlideInput, maxHoldSlideInput, minHoldVelocityInput, maxHoldVelocityInput) {
	// set all the variables to use whatever the input technique sends
	minHoldSlide = minHoldSlideInput;
	maxHoldSlide = maxHoldSlideInput;
	minBottomSlide = minBottomSlideInput;
	maxBottomSlide = maxBottomSlideInput;
	minTopSlide = minTopSlideInput;
	maxTopSlide = maxTopSlideInput;
	minHoldVelocity = minHoldVelocityInput
	maxHoldVelocity = maxHoldVelocityInput
	minVelocity = minVelocityInput;
	maxVelocity = maxVelocityInput;
	
	clearAllTimeouts();
	
	console.log("Loading New Technique");
	
	// set the slide value and wait for completion of the async function
	let slidePromise = new Promise((resolve, reject) => {
		resolve(setSlide());
	});
	let slideResult = await slidePromise;
	
	// set the velocity value and wait for completion of the async function
	let velocityPromise = new Promise((resolve, reject) => {
		resolve(setVelocity());
	});
	let velocityResult = await velocityPromise;	
	
	console.log("New Technique Loaded");
};
*/

// uses absoluteMax and absoluteMin values to return a recalibrated Slide value
function calculateSlideValue(slideValue) {
	let absoluteMin = parseFloat(document.getElementById("absoluteMin").value);
	let absoluteMax = parseFloat(document.getElementById("absoluteMax").value);
	let newSlideValue = ((absoluteMax - absoluteMin) * (slideValue / 100)) + absoluteMin;
	return 	newSlideValue;
};

// returns an inclusive random number between a min and max set of numbers
function getRandomNumber(min, max) {
	return (Math.floor((Math.random() * (max - min + 1)) + min));
};

async function turnOn() {	
	// set the mode and wait for completion of the async function
	let modePromise = new Promise((resolve, reject) => {
		resolve(setMode(0));
	});
	let modeResult = await modePromise;
	
	startStroke();
};

// Returns the current mode to the console
function getMode() {
	let connectionKey = document.getElementById("connectionKey").value
	request.open('GET', `${baseURI}${modeAPI}`, true);
	request.setRequestHeader('accept', 'application/json');
	request.setRequestHeader('X-Connection-Key',`${connectionKey}`);

	request.onload = function () {
	  // Begin accessing JSON data here
	  let data = JSON.parse(this.response)

	  if (request.status >= 200 && request.status < 400) {
		console.log(`Current mode is `, data.mode)
	  } else {
		console.log(data.message)
	  }
	}	
	request.send();
};

// Sets the Handy mode to 0 - HAMP mode.  This is needed prior to starting HAMP functions
async function setMode(mode) {
	await new Promise((resolve, reject) => {

		let connectionKey = document.getElementById("connectionKey").value
		request.open('PUT', `${baseURI}${modeAPI}`, true);
		request.setRequestHeader('accept', 'application/json');
		request.setRequestHeader('X-Connection-Key', `${connectionKey}`);
		request.setRequestHeader('Content-Type', 'application/json');

		// Data to send
		var data = `{
			"mode": ${mode}
		}`;

		request.onload = function () {
		// Begin accessing JSON data here
		let data = JSON.parse(this.response)

		if (request.status >= 200 && request.status < 400) {
			console.log(`Mode is set to 0`)
			resolve(data.result);
			} else {
			console.log(data.message)
			reject(data.message);
			}
		}
		request.send(data);

	});
	return 0;
	
};

function startStroke() {
	clearAllTimeouts();
	
	let connectionKey = document.getElementById("connectionKey").value
	request.open('PUT', `${baseURI}${hampStartAPI}`, true);
	request.setRequestHeader('accept', 'application/json');
	request.setRequestHeader('X-Connection-Key', `${connectionKey}`);

	request.onload = function () {
		// Begin accessing JSON data here
		let data = JSON.parse(this.response);

		if (request.status >= 200 && request.status < 400) {
			console.log(`Stroke is active`);
		} else {
			console.log(data.message);
		};
	};
	
	request.send();
};

function stopStroke() {
	clearAllTimeouts();
	
	let connectionKey = document.getElementById("connectionKey").value
	request.open('PUT', `${baseURI}${hampStopAPI}`, true);
	request.setRequestHeader('accept', 'application/json');
	request.setRequestHeader('X-Connection-Key', `${connectionKey}`);	

	request.onload = function () {
		// Begin accessing JSON data here
		let data = JSON.parse(this.response);

		if (request.status >= 200 && request.status < 400) {
			console.log(`Stroke is inactive`);
			} else {
			console.log(data.message);
		};
	};
	
	request.send();
};

function getVelocity() {
	let connectionKey = document.getElementById("connectionKey").value
	request.open('GET', `${baseURI}${hampVelocityAPI}`, true);
	request.setRequestHeader('accept', 'application/json');
	request.setRequestHeader('X-Connection-Key',`${connectionKey}`);

	request.onload = function () {
		// Begin accessing JSON data here
		let data = JSON.parse(this.response);

		if (request.status >= 200 && request.status < 400) {
			console.log(`Current speed is `, data.velocity);
		} else {
			console.log(data.message);
		};
	};
	
	request.send();
};

async function setVelocity() {
	let holdVelocity = getRandomNumber(minHoldVelocity, maxHoldVelocity);
	let velocityRandomize;
	clearTimeout(velocityRandomize);
	velocityRandomize = setTimeout(setVelocity, holdVelocity);
	await new Promise((resolve, reject) => {
		let connectionKey = document.getElementById("connectionKey").value
		request.open('PUT', `${baseURI}${hampVelocityAPI}`, true);
		request.setRequestHeader('accept', 'application/json');
		request.setRequestHeader('X-Connection-Key', `${connectionKey}`);
		request.setRequestHeader('Content-Type', 'application/json');
		let velocity = getRandomNumber(minVelocity, maxVelocity)
		// Data to send
		let velocityData = `{
			"velocity": ${velocity}
		}`;

		request.onload = function () {
			// Begin accessing JSON data here
			let data = JSON.parse(this.response);

			if (request.status >= 200 && request.status < 400) {
				//console.log(data.result);
				console.log(`velocity set to ${velocity}`)
				resolve(data.result);
			} else {
				//console.log(data.message);
				console.log("velocity failed to set")
				reject(data.message);
			};
		};
		
		request.send(velocityData);
	});
	
	return 0;
};

async function setSlide() {
	let holdSlide = getRandomNumber(minHoldSlide, maxHoldSlide);
	let slideRandomize;
	clearTimeout(slideRandomize);
	slideRandomize = setTimeout(setSlide, holdSlide);
	await new Promise((resolve, reject) => {
		let connectionKey = document.getElementById("connectionKey").value
		request.open('PUT', `${baseURI}${slideAPI}`, true);
		request.setRequestHeader('accept', 'application/json');
		request.setRequestHeader('X-Connection-Key', `${connectionKey}`);
		request.setRequestHeader('Content-Type', 'application/json');

		let newMin = calculateSlideValue(getRandomNumber(minBottomSlide, maxBottomSlide));
		let newMax = calculateSlideValue(getRandomNumber(minTopSlide, maxTopSlide));

		// Data to send
		let slideValues = `{
			"min": ${newMin},
			"max": ${newMax}
		}`;

		request.onload = async function () {
			// Begin accessing JSON data here
			let data = JSON.parse(this.response);

			if (request.status >= 200 && request.status < 400) {
				//console.log(data.result);
				console.log(`slide value set from ${newMin} to ${newMax}`);
				resolve(data.result);
			} else {
				//console.log(data.message);
				console.log("slide value failed to set");
				reject(data.message);
			};
		};
		
		request.send(slideValues);
	});
	
	return 0;
};

function createClearAllTimeouts() {
	const noop = () => {};
	let firstId = setTimeout(noop, 0);

	return () => {
		const lastId = setTimeout(noop, 0);
		while (firstId !== lastId) {
			firstId += 1;
			clearTimeout(firstId);
		}
	};
};



